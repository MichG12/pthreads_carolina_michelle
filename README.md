Tarea Programación Paralela

Michelle Gutiérrez Muñoz B4319
Carolina Paniagua Peñaranda B65268 
## How to build the project
Create a build directory and run all targets there
```
>> mkdir build
>> cd build
>> cmake ..
>> make <target> (pth)
```

## How to run the simulation
The simulation executable is located inside the build directory (src/pth)

