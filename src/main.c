#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>
#include <math.h>
#include "../include/project-pthreads.h"
 

// Variables globales
int n;
long* matriz;
long* vector;
long* result_serie;
long* result_paralelo; 
static double suma = 0;
static double resultado_serie_pi;

 // MUTEX para sincronizar threads
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

// Funciones serie y pthreads
void multiplicacion_serie(int n, long* result_serie){
    long * index = result_serie;
    for (int i = 0; i < n; i++) {
        result_serie += i; 
	    *result_serie = 0; //initialize value to 0
	    for (int j = 0; j < n; j++) {
	        *result_serie += matriz[i*n+j] * vector[j]; //add  Matriz[i,j]xVector[i]  
	    }
        result_serie = index; 
    }
}

void *Pth_multiplicacion(void* thread_number) {
   long line = (long) thread_number;
   result_paralelo[line] = 0;
   for (int column = 0 ; column < n; column++){
       result_paralelo[line] += matriz[line*n+column]*vector[column];
   }
   return NULL;
}

double aproxpi_serie (){
      double sumas =0;
      int n = 10000;
      double aprox;
    for (int i=1;i<=n;i++){
        aprox=(-4)*pow(-1,i)/(2*i-1);
        sumas = sumas + aprox;
    }
    return sumas;
}

void *thread_routine1(void *arg){
 int n1 = 5000;
 double aprox1;

    for (int i=1;i<=n1;i++){
        aprox1=(-4)*pow(-1,i)/(2*i-1);
         pthread_mutex_lock(&mutex);
         suma = suma + aprox1;
         pthread_mutex_unlock(&mutex);
       
    }
}

void *thread_routine2(void *arg){
 int n2 = 10000;
 double aprox2;

    for (int i=5001;i<=n2;i++){
        aprox2=(-4)*pow(-1,i)/(2*i-1);
         pthread_mutex_lock(&mutex);
         suma = suma + aprox2;
         pthread_mutex_unlock(&mutex);
    }   
}




int main(int argc, char const *argv[]){
    // Resultado de aproximación de pi en serie
    //double resultado_serie_pi;
    resultado_serie_pi = aproxpi_serie();

   // Declarar threads para paralelizar aprox PI
      pthread_t thread1;
      pthread_t thread2;

    // Crear pthreads para paralelizar aprox PI
    pthread_create(&thread1,NULL,thread_routine1,NULL);
    pthread_create(&thread2,NULL,thread_routine2,NULL);

    pthread_join(thread1,NULL);
    pthread_join(thread2,NULL);


    printf("Valor aproximado de PI (SERIE):  %f\n ", resultado_serie_pi);

    printf("Valor aproximado de PI (PARALELO):  %f\n ", suma);

    long thread; 
    pthread_t* thread_handles;
    int vectors_are_equal = 0; 
    srand(time(0)); //semilla para generar valores aleatorios
    n = rand()%((NMAX+1)-NMIN) + NMIN;; //numero aleatorio n 
    printf("Size: %d\n", n);
    matriz = malloc(n*n*sizeof(long));
    vector = malloc(n*sizeof(long));
    result_serie = malloc(n*sizeof(long));
    result_paralelo = malloc(n*sizeof(long));
    
    thread_handles = malloc(n*sizeof(pthread_t));


  
    printf("Vector:");
    printf("%c", '[');
    //print vector
    for (int i = 0; i < n; i++)
    {
        vector[i] = rand()%100; //fill vector
        printf("%ld", vector[i]);
        if(i != n-1){
            printf("%c", ',');
        }
    }
    printf("%c\n", ']');

    printf("Matriz: \n");
    //print matrix
    printf("%c", '[');
    for (int i = 0; i < n; i++ ) {
      printf("%c", '[');
      for (int j = 0; j < n; j++ ) {
        matriz[i*n+j] = rand()%100; //fill matrix
        printf("%ld", matriz[i*n+j]);
        if (j != n-1){
            printf("%c", ',');
        }
        else{
            printf("%c\n", ']');
        }
      }

        if (i != n-1){
            printf("%c", ',');
        }
        else
        {
            printf("%c\n", ']');
        }
   }
 
    multiplicacion_serie(n, &result_serie[0]);

    for (thread= 0; thread < n; thread++){
        pthread_create(&thread_handles[thread], NULL,Pth_multiplicacion, (void*) thread);
    }

   for (thread = 0; thread < n; thread++){
      pthread_join(thread_handles[thread], NULL);
   }


    //print result
    printf("Resultado serie:");
    printf("%c", '[');
    //print vector
    for (int i = 0; i < n; i++)
    {
        printf("%ld", result_serie[i]);
        if(i != n-1){
            printf("%c", ',');
        }
    }
    printf("%c\n", ']');

    printf("Resultado paralelo:");
     printf("%c", '[');
    //print vector
    for (int i = 0; i < n; i++)
    {
        printf("%ld", result_paralelo[i]);
        if(i != n-1){
            printf("%c", ',');
        }
    }
    printf("%c\n", ']');
    
    //Compare results

    for (int i = 0; i < n; i++)
    {
        if(result_paralelo[i] == result_serie[i]){
            vectors_are_equal = 1;
        }
        else{
            vectors_are_equal = 0; 
            break;
        }
    }
    if(vectors_are_equal){
        printf("Resultados son iguales\n"); 
       return PTH_SUCCESS;
    }
    
    return PTH_FAILED;

}