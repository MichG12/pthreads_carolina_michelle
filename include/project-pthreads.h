#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>

//variables globales
extern int n;
extern long* matriz;
extern long* vector;
extern long* result_serie;
extern long* result_paralel;

//Codigos de error
enum PTH_error_codes {
  PTH_SUCCESS       = 0,
  PTH_FAILED = -1,
};


#define NMIN 3
#define NMAX 8



/**
 * multiplicacion_serie
 * Toma una matriz y un vector (variables globales) y los multiplica
 * 
 *
 * @param [in]  n               es el tamaño del vector y matriz
 * @param [out] result_serie    es el puntero al vector de resultado
 *
 * @returns void         
 *                            
 */
void multiplicacion_serie(int n, long* result_serie);

/**
 * Pth multiplicacion
 * Multiplica la fila de una matriz correspondiente al numero de thread 
 * por el vector de tamaño n
 *
 * @param [in]  thread_number   el numero de thread encargado de esta multiplicacion
 *     
 *
 * @returns void         
 *                            
 */
void *Pth_multiplicacion(void* thread_number);